<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Prepare data for testing
     * 
     * @param array  $partnerBlueprint
     * @param array  $orderBlueprint
     * @param array  $product1Blueprint
     * @param array  $product2Blueprint
     * @param int  $quantity2
     * 
     * @return array
     */
    protected function generateOrderSet(
        $partnerBlueprint = [],
        $orderBlueprint = [],
        $product1Blueprint = [],
        $product2Blueprint = [],
        $product1Quantity = 1,
        $product2Quantity = 1
    )
    {
        $partner = factory('App\Partner')->create($partnerBlueprint);

        $order = factory('App\Order')->create(
            array_merge(['partner_id' => $partner->id], $orderBlueprint)
        );

        $vendor1 = factory('App\Vendor')->create();
        $vendor2 = factory('App\Vendor')->create();

        $product1 = factory('App\Product')->create(
            array_merge(['vendor_id' => $vendor1], $product1Blueprint)
        );
        $product2 = factory('App\Product')->create(
            array_merge(['vendor_id' => $vendor2], $product2Blueprint)
        );

        $orderProduct1 = factory('App\OrderProduct')->create([
            'order_id' => $order->id,
            'product_id' => $product1->id,
            'quantity' => $product1Quantity,
            'price' => $product1->price,
            'created_at' => $order->created_at,
            'updated_at' => $order->updated_at
        ]);

        $orderProduct2 = factory('App\OrderProduct')->create([
            'order_id' => $order->id,
            'product_id' => $product2->id,
            'quantity' => $product2Quantity,
            'price' => $product2->price,
            'created_at' => $order->created_at,
            'updated_at' => $order->updated_at
        ]);

        return [
            'partner' => $partner, 
            'order' => $order, 
            'products' => [$product1, $product2], 
            'order_products' => [$orderProduct1, $orderProduct2]
        ];
    }
}
