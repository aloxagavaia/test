<?php

namespace Tests\Unit;

use App\Order;
use Tests\TestCase;
use App\Filters\OrderFilters;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_checks_the_correctnes_of_culculated_price_for_getAll_query()
    {
        $partnerBlueprint = [];
        $orderBlueprint = [];
        $product1Blueprint = ['price' => 1000];
        $product2Blueprint = ['price' => 500];
        $product1Quantity = 5;
        $product2Quantity = 3;

        $this->generateOrderSet(
            $partnerBlueprint, 
            $orderBlueprint, 
            $product1Blueprint, 
            $product2Blueprint, 
            $product1Quantity, 
            $product2Quantity
        );
        
        $order = Order::getAll(
            new OrderFilters(new Request())
        )->first();

        $this->assertEquals(6500, $order->order_price);
    }

    /** @test */
    public function it_checks_the_correctnes_of_culculated_price_for_getById_query()
    {
        $partnerBlueprint = [];
        $orderBlueprint = [];
        $product1Blueprint = ['price' => 1000];
        $product2Blueprint = ['price' => 500];
        $product1Quantity = 2;
        $product2Quantity = 2;

        $orderSet = $this->generateOrderSet(
            $partnerBlueprint, 
            $orderBlueprint, 
            $product1Blueprint, 
            $product2Blueprint, 
            $product1Quantity, 
            $product2Quantity
        );

        $order = Order::getById($orderSet['order']->id)->first();

        $this->assertEquals(3000, $order->order_price);
    }
}
