<?php

namespace Tests\Unit;

use App\Order;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderFilterTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_checks_the_take_filter()
    {
        $orderSet1 = $this->generateOrderSet();
        $orderSet2 = $this->generateOrderSet();

        $this->get(route('orders.index', ['take' => 1]))
             ->assertSee("Order {$orderSet1['order']->id}")
             ->assertDontSee("Order {$orderSet2['order']->id}");
    }

    /** @test */
    public function it_checks_the_orderByDelivery_filter()
    {
        $order1Blueprint = ['delivery_dt' => Carbon::now()->subHour()];
        $orderSet1 = $this->generateOrderSet([], $order1Blueprint);

        $order2Blueprint = ['delivery_dt' => Carbon::now()];
        $orderSet2 = $this->generateOrderSet([], $order2Blueprint);

        $this->get(route('orders.index', ['take' => 1, 'orderByDelivery' => 'ASC']))
             ->assertSee("Order {$orderSet1['order']->id}")
             ->assertDontSee("Order {$orderSet2['order']->id}");

        $this->get(route('orders.index', ['take' => 1, 'orderByDelivery' => 'DESC']))
             ->assertDontSee("Order {$orderSet1['order']->id}")
             ->assertSee("Order {$orderSet2['order']->id}");
    }

    /** @test */
    public function it_checks_the_status_filter()
    {
        $order1Blueprint = ['status' => 0];
        $orderSet1 = $this->generateOrderSet([], $order1Blueprint);

        $order2Blueprint = ['status' => 10];
        $orderSet2 = $this->generateOrderSet([], $order2Blueprint);

        $order3Blueprint = ['status' => 20];
        $orderSet3 = $this->generateOrderSet([], $order3Blueprint);

        $this->get(route('orders.index', ['status' => 0]))
             ->assertSee("Order {$orderSet1['order']->id}")
             ->assertDontSee("Order {$orderSet2['order']->id}")
             ->assertDontSee("Order {$orderSet3['order']->id}");

        $this->get(route('orders.index', ['status' => 10]))
             ->assertDontSee("Order {$orderSet1['order']->id}")
             ->assertSee("Order {$orderSet2['order']->id}")
             ->assertDontSee("Order {$orderSet3['order']->id}");

        $this->get(route('orders.index', ['status' => 20]))
             ->assertDontSee("Order {$orderSet1['order']->id}")
             ->assertDontSee("Order {$orderSet2['order']->id}")
             ->assertSee("Order {$orderSet3['order']->id}");
    }

    /** @test */
    public function it_checks_the_delivery_filter()
    {
        $order1Blueprint = ['delivery_dt' => Carbon::now()->subHour()];
        $orderSet1 = $this->generateOrderSet([], $order1Blueprint);

        $order2Blueprint = ['delivery_dt' => Carbon::now()->addHour()];
        $orderSet2 = $this->generateOrderSet([], $order2Blueprint);

        $order3Blueprint = ['delivery_dt' => Carbon::now()->addDay()];
        $orderSet3 = $this->generateOrderSet([], $order3Blueprint);

        // Filter expired orders
        $this->get(route('orders.index', ['delivery' => 'expired']))
             ->assertSee("Order {$orderSet1['order']->id}")
             ->assertDontSee("Order {$orderSet2['order']->id}")
             ->assertDontSee("Order {$orderSet3['order']->id}");

        // Filter orders waiting to be dispatched 
        $this->get(route('orders.index', ['delivery' => 'future']))
             ->assertDontSee("Order {$orderSet1['order']->id}")
             ->assertSee("Order {$orderSet2['order']->id}")
             ->assertSee("Order {$orderSet3['order']->id}");

        // Filter orders waiting to be dispatched today
        $this->get(route('orders.index', ['delivery' => 'today']))
             ->assertSee("Order {$orderSet1['order']->id}")
             ->assertSee("Order {$orderSet2['order']->id}")
             ->assertDontSee("Order {$orderSet3['order']->id}");

        // Filter orders waiting to be dispatched within 24 hours
        $this->get(route('orders.index', ['delivery' => 'hours24']))
             ->assertDontSee("Order {$orderSet1['order']->id}")
             ->assertSee("Order {$orderSet2['order']->id}")
             ->assertSee("Order {$orderSet3['order']->id}");
    }
}
