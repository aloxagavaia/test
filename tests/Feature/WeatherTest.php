<?php

namespace Tests\Feature;

use GuzzleHttp\Client;
use Tests\TestCase;

class WeatherTest extends TestCase
{
    /** @test */
    public function a_user_can_see_the_temperature_of_a_town_that_exists_in_database()
    {
        $response = $this->get('weather/show/bryansk');

        $response->assertStatus(200);

        $response->assertSee('Current temperature:');
    }

    /** @test */
    public function a_user_cannot_see_the_temperature_of_a_town_that_does_not_exists_in_database()
    {
        $response = $this->get('weather/show/UnknownTown');
        
        $response->assertDontSee('Current temperature:');

        $response->assertStatus(404);
    }
}
