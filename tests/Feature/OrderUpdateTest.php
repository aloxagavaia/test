<?php

namespace Tests\Feature;

use App\Mail\OrderFinished;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;

class OrderUpdateTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_update_an_existing_order()
    {
        Mail::fake();

        $partnerBlueprint = [];
        $orderBlueprint = ['status' => 0];

        $orderSet = $this->generateOrderSet($partnerBlueprint, $orderBlueprint);

        $newPartner = factory('App\Partner')->create();

        $newData = [
            'client_email' => 'new@gmail.com',
            'partner_id' => $newPartner->id,
            'status' => 10
        ];

        $this->patch(route('orders.update', $orderSet['order']), $newData)
             ->assertStatus(204);

        $this->assertDatabaseHas('orders', $newData);

        Mail::assertNotSent(OrderFinished::class);
        
        $newData['status'] = 20;

        $this->patch(route('orders.update', $orderSet['order']), $newData)
             ->assertStatus(204);
             
        Mail::assertSent(OrderFinished::class);
    }

    /** @test */
    public function a_user_cannot_update_a_nonexistent_order()
    {
        $this->patch(route('orders.update', 99999), [])
             ->assertStatus(404);
    }

    /** @test */
    public function a_user_has_to_provide_required_information_to_update_an_order()
    {
        $orderSet = $this->generateOrderSet();

        $this->patch(route('orders.update', $orderSet['order']), [])
             ->assertStatus(302)
             ->assertSessionHasErrors([
                 'client_email', 'partner_id', 'status'
             ]);
    }
}
