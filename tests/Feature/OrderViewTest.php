<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderViewTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_view_all_orders()
    {
        $orderSet1 = $this->generateOrderSet();
        $orderSet2 = $this->generateOrderSet();
        
        $this->get(route('orders.index'))
             ->assertSee('Order ' . $orderSet1['order']->id)
             ->assertSee('Order ' . $orderSet2['order']->id);
    }

    /** @test */
    public function a_user_can_view_edit_order_page()
    {
        $orderSet = $this->generateOrderSet();

        $this->get(route('orders.edit', $orderSet['order']))
             ->assertSee($orderSet['order']->clientEmail)
             ->assertSee($orderSet['partner']->name)
             ->assertSee($orderSet['order_products'][0]->name)
             ->assertSee($orderSet['order_products'][1]->name);
    }

    /** @test */
    public function a_user_cannot_view_a_nonexistent_order()
    {
        $this->get(route('orders.edit', 1))
             ->assertStatus(404);
    }
}
