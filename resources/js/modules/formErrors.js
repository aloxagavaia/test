export default class formErrors 
{
    constructor(errors) {
        this.errors = errors;
    }

    /**
     * Record new errors
     * 
     * @param {object} errors 
     */
    record(errors) {
        this.errors = errors;
    }
    
    /**
     * Clear the error for the given field
     * 
     * @param {string} field 
     */
    clear(field) {
        if(field) {
            delete this.errors[field];

            return;
        }

        this.errors = {};
    }

    /**
     * Check if an error exists for a given field
     * 
     * @param {string} field
     * @return bool
     */
    has(field) {
        return this.errors.hasOwnProperty(field);
    }

    /**
     * Retrieve the error message for a field
     * 
     * @param {string} field 
     * @return {string}
     */
    get(field) {
        if(this.errors[field]) {
            return this.errors[field][0];
        }
    }

    /**
     * Define if the error object has any errors
     * 
     * @return {bool}
     */
    any() {
        return Object.keys(this.errors).length > 0;
    }
}