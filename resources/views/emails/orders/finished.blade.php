@component('mail::message')
# Order {{ $order->id }} has been finished!

Here are the order products:
@foreach ($products as $product)
- {{ $product->name }}  (quantity: {{ $product->quantity }})
@endforeach

Total order price: {{ $products->sum('total_sum') }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
