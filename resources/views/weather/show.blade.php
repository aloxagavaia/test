@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">   
        <div class="offset-md-2 col-md-8">
            <div class="mt-5">
                <div class="d-flex flex-column text-center">
                    <h1>{{ ucfirst($town) }}</h1>
                    <h2>
                        @if ($error_message)
                            {{ $error_message }}
                        @else
                            Current temperature: {{ $data->fact->temp }} °C
                        @endif
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection