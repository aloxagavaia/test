@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">   
        <div class="offset-md-4 col-md-4">
            <div class="mt-5">
                <div class="d-flex flex-column">
                    <a href="{{ route('weather.show', 'bryansk') }}" class="btn btn-primary mb-3">Weather in Bryansk</a>
                    <a href="{{ route('orders.index') }}" class="btn btn-success">Watch orders</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection