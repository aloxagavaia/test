@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">   
        <div class="offset-md-2 col-md-8">
            <div class="d-flex flex-row justify-content-around mt-5">
                <a href="{{ route('orders.index', [
                    'delivery' => 'expired', 
                    'status' => 10, 
                    'orderByDelivery' => 'DESC', 
                    'take' => 50
                ]) }}">Expired</a>

                <a href="{{ route('orders.index', [
                    'delivery' => 'hours24', 
                    'status' => 10, 
                    'orderByDelivery' => 'ASC'
                ]) }}">Current</a>

                <a href="{{ route('orders.index', [
                    'delivery' => 'future', 
                    'status' => 0, 
                    'orderByDelivery' => 'ASC', 
                    'take' => 50
                ]) }}">New</a>

                <a href="{{ route('orders.index', [
                    'delivery' => 'today', 
                    'status' => 20, 
                    'orderByDelivery' => 'DESC', 
                    'take' => 50
                ]) }}">Delivered</a>
            </div>

            <div class="grid">
                <div class="table-row">Order ID</div>
                <div class="table-row">Partner</div>
                <div class="table-row">Price</div>
                <div class="table-row">Products</div>
                <div class="table-row">Status</div>
            
                @foreach ($orders as $order)
                    <div class="table-row">
                        <a href="{{ route('orders.edit', $order) }}" target="_blank">Order {{ $order->id }}</a>
                    </div>
                    <div class="table-row">{{ $order->partner_name }}</div>
                    <div class="table-row">{{ $order->order_price }}</div>
                    <div class="table-row">
                        <span class="text-muted">
                            <ul>
                            @foreach ($order->products as $product)
                                <li>{{ $product->name }}</li>
                            @endforeach
                            </ul>
                        </span>
                    </div>
                    <div class="table-row">{{ $order->status() }}</div>
                @endforeach
            </div>

        </div>
    </div>
</div>

@endsection