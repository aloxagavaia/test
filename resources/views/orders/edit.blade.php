@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">   
        <div class="offset-md-3 col-md-6">
            <div class="mt-5">
                <div class="d-flex flex-column">
                    <h1>Edit Order</h1>
                    <edit-order 
                        :order="{{ $order }}"
                        :products="{{ $products }}"
                        :partners="{{ $partners }}"
                    ></edit-order>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection