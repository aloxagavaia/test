<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.app_headinfo')
</head>

<body>  
    <div id="app" class="vh-100 d-flex flex-column">
        <!-- Navigation -->
        <nav class="navbar navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home') }}">Test</a>
            </div>
        </nav>

        <!-- Page Content -->
        <main id="app" class="flex-grow-1">                 
            @yield('content')
        </main>

        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; Test 2020</p>
            </div>
        </footer>
    </div>

    <script src="{{ asset('js/app.js') }}" defer></script>    
</body>
</html>
