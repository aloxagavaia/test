<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {

    $createdAt = \Carbon\Carbon::now()->subDays(rand(0, 4));
    
    return [
        'status' => $faker->randomElement([0, 10, 20]),
        'client_email' => $faker->email,
        'partner_id' => function() {
            return factory('App\Partner')->create()->first()->id;
        },
        'delivery_dt' => $createdAt->copy()->addHours(rand(6,50)),
        'created_at' => $createdAt,
        'updated_at' => $createdAt->copy()->addHours(rand(1,5)),
    ];
});
