<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => 'Product_' . $faker->randomNumber(),
        'price' => $faker->numberBetween(100, 1000),
        'vendor_id' => $faker->numberBetween(1,10),
    ];
});
