<?php

use Faker\Generator as Faker;

$factory->define(App\OrderProduct::class, function (Faker $faker) {
    return [
        'order_id' => '',
        'product_id' => '',
        'quantity' => rand(1,3),
        'price' => $faker->numberBetween(100, 1000),
        'created_at' => '',
        'updated_at' => '',
    ];
});