<?php

namespace App;

use App\Exceptions\WeatherException;
use GuzzleHttp\Client;

abstract class WeatherClient
{
    /**
     * A client to make outgoing HTTP requests.
     */
    protected $client;

    public function __construct()
    {
        $this->client = new Client(['headers' => $this->setHeaders()]);
    }

    /**
     * The array with towns coordinates.
     */
    protected $towns = [
        'bryansk' => [
            'lat' => '53.243562',
            'lon' => '34.363407'
        ]
    ];

    /**
     * Set an API url.
     * 
     * @param string  $town
     * @return string
     */
    abstract protected function setUrl($town);

    /**
     * Set the necessary headers.
     * 
     * @return array
     */
    abstract protected function setHeaders();

    /**
     * Get the weather infromation for the given town.
     * 
     * @param string  $town
     * @return object
     */
    public function getWeather($town)
    {
        $this->check($town);

        $url = $this->setUrl($town);

        $weatherResponse = $this->get($url);

        if($weatherResponse->getStatusCode() !== 200) {
            throw WeatherException::badStatusCode($town);
        }

        return $this->getDataFromResponse($weatherResponse);
    }

    /**
     * Send the get request to the given url.
     * 
     * ! If the API key is incorrect an exception will be 
     * thrown. Thus, the client->request(..) should be
     * enclosed in try catch block. For the test
     * task I'll omit this.
     * 
     * @param string  $url
     * @return \GuzzleHttp\Psr7\Response
     */
    protected function get($url)
    {
        return $this->client->request('GET', $url);
    }

    /**
     * Check if the given town exists.
     * 
     * ! Another option is to put towns and their coordinates 
     * in database and check during the validation in the
     * controller.
     * 
     * @param string  $town
     * @return void
     */
    protected function check($town)
    {
        if(! array_key_exists($town, $this->towns)) {
            throw WeatherException::townDoesNotExist($town);
        }
    }

    /**
     * Get the weather information from the response.
     * 
     * @param \GuzzleHttp\Psr7\Response  $response
     * @return object
     */
    protected function getDataFromResponse($response)
    { 
        $body = $response->getBody();

        $content = $body->getContents();

        return json_decode($content);
    }
}