<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;

class WeatherException extends Exception
{
    /**
     * A town associated with an exception
     */
    protected $town;

    /**
     * Exception message
     */
    protected $message;

    /**
     * Exception status code
     */
    protected $code;

    public function __construct($town, $message, $code)
    {
        $this->town = $town;

        $this->message = $message;

        $this->code = $code;
    }

    /**
     * Throws an exception when the status code
     * is not equal 200
     * 
     * @return  \App\Exceptions\WeatherException
     */
    public static function badStatusCode($town)
    {
        return new static(
            $town,
            'Oops! Something bad happended. Please try again later :(', 
            400
        );
    }

    /**
     * Throws an exception if town does not exists
     * 
     * @return  \App\Exceptions\WeatherException
     */
    public static function townDoesNotExist($town)
    {
        return new static(
            $town,
            'Oops! This town does not exist :(', 
            404
        );
    }

    public function render()
    {
        return response()->view('weather.show', [
            'town' => $this->town,
            'data' => '',
            'error_message' => $this->message
        ], $this->code);
    }
}