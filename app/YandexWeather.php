<?php

namespace App;

class YandexWeather extends WeatherClient
{
    /**
     * Basic URL for API requests
     */
    protected $url = 'https://api.weather.yandex.ru/v2/forecast/';

    /**
     * Set an API url.
     * 
     * @param string  $town
     * @return string
     */
    public function setUrl($town)
    {
        $lat = $this->towns[$town]['lat'];
        $lon = $this->towns[$town]['lon'];

        return "{$this->url}?lat={$lat}&lon={$lon}";
    }

    /**
     * Set the necessary headers.
     * 
     * @return array
     */
    public function setHeaders()
    { 
        return ['X-Yandex-API-Key' => config('services.yandex_weather.key')];
    }
}