<?php

namespace App\Listeners;

use App\Events\OrderUpdated;
use App\Mail\OrderFinished;
use Illuminate\Support\Facades\Mail;

class SendEmailNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderUpdated  $event
     * @return void
     */
    public function handle(OrderUpdated $event)
    {
        /**
         * It's better to configure Laravel queues
         * and use ->queue(...) instead of ->send(...)
         */
        if($event->order->status == 20) {
            Mail::to($this->getReceiversEmails($event->order))
                ->send(new OrderFinished($event->order));
        }
    }

    /**
     * Get receivers email addresses of the order.
     * 
     * @return array
     */
    protected function getReceiversEmails($order)
    {
        // Get vendors emails
        $receivers = $order->products()
                           ->with('vendor')
                           ->get()
                           ->pluck('vendor.email')
                           ->toArray();
       
        // Add partner email
        $receivers[] = $order->partner->email;
        
        return $receivers;
    }
}
