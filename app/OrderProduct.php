<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderProduct extends Model
{
    /**
     * Get information about order products.
     * 
     * @param int $id
     * @return \Illuminate\Support\Collection
     */
    public static function getByOrderId($id)
    {
        return DB::table('order_products')
                    ->select('order_products.product_id', 'order_products.quantity', 'products.name')
                    ->selectRaw('(order_products.price * order_products.quantity) as total_sum')
                    ->join('products', 'order_products.product_id', '=', 'products.id')
                    ->where('order_products.order_id', $id)
                    ->get();
    }
}
