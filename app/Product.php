<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * A product belongs to a vendor.
     */
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }
}
