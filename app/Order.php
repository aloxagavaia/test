<?php

namespace App;

use App\Events\OrderUpdated;
use App\Filters\OrderFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $fillable = ['client_email', 'partner_id', 'status'];

    /**
     * The booted method of the model.
     * 
     * @return void
     */
    protected static function booted()
    {
        static::updated(function ($order) {
            OrderUpdated::dispatch($order);
        });
    }

    /**
     * Available states.
     */
    protected $states = [
        0  => 'New',
        10 => 'Confirmed',
        20 => 'Finished'
    ];

    /**
     * An order has many products.
     */
    public function products()
    {
        return $this->hasManyThrough(
            'App\Product', 
            'App\OrderProduct',
            'order_id',
            'id',
            'id',
            'product_id'
        );
    }

    /**
     * An order belongs to a partner.
     */
    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

    /**
     * Get available order states.
     * 
     * @return array
     */
    public function getStates()
    {
        return array_keys($this->states);
    }

    /**
     * Get the status of an order.
     * 
     * @return string
     */
    public function status()
    {
        return $this->states[$this->status];
    }

    /**
     * Get information about order products.
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function productsInfo()
    {
        return OrderProduct::getByOrderId($this->id);
    }

    /**
     * Get all the orders with associated data.
     * 
     * @param \App\Filters\OrderFilters  $filters 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAll(OrderFilters $filters)
    {
        $query = DB::table('orders')
                    ->select('orders.id', 'partners.name as partner_name', 'orders.status')
                    ->selectRaw('SUM(order_products.quantity * order_products.price) as order_price')
                    ->leftJoin('order_products', 'orders.id', '=', 'order_products.order_id')
                    ->leftJoin('partners', 'orders.partner_id', '=', 'partners.id')
                    ->groupBy('orders.id');
        
        $orders = $filters->apply($query)->get();

        return static::hydrate($orders->toArray());
    }

    /**
     * Get the order and associated data by id.
     * 
     * @param int  $id
     * @return \Illuminate\Support\Collection
     */
    public static function getById($id)
    {
        $orders = DB::table('orders')
                    ->select(
                        'orders.id', 
                        'orders.client_email', 
                        'partners.id as partner_id', 
                        'partners.name as partner_name',
                        'orders.status')
                    ->selectRaw('SUM(order_products.quantity * order_products.price) as order_price')
                    ->leftJoin('order_products', 'orders.id', '=', 'order_products.order_id')
                    ->leftJoin('partners', 'orders.partner_id', '=', 'partners.id')
                    ->where('orders.id', $id)
                    ->groupBy('orders.id')
                    ->get();
        
        return static::hydrate($orders->toArray());
    }
}
