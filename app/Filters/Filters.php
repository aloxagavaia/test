<?php

namespace App\Filters;

use Illuminate\Http\Request;

abstract class Filters
{
    protected $request, $builder;

    protected $filters = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply filters
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply($builder)
    {
        $this->builder = $builder;

        if(! empty($filters = $this->getFilters()))
        {
            foreach ($filters as $filter => $value) {
                if(method_exists($this, $filter))
                {
                    $this->$filter($value);
                }
            }
        }    
        
        return $this->builder;
    }

    /**
     * Get all filters
     * 
     * @return  array
     */
    protected function getFilters()
    {
        $requestFilters = array_intersect(
            array_keys($this->request->all()), $this->filters 
        );

        return $this->request->only($requestFilters);        
    }
}