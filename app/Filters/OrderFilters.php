<?php

namespace App\Filters;

use App\Filters\Filters;
use Carbon\Carbon;

class OrderFilters extends Filters
{
    protected $filters = ['delivery', 'take', 'orderByDelivery', 'status'];

    /**
     * Take the specific number of records
     * 
     * @param int  $number
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function take($number = 10)
    {
        return $this->builder->take($number);
    }

    /**
     * Filter orders by "delivery_dt" field 
     * in asc and desc order
     * 
     * @param string  $order
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function orderByDelivery($order = 'DESC')
    {
        return $this->builder->orderBy('delivery_dt', $order);
    }

    /**
     * Filter orders by "status" field 
     * 
     * @param int  $status
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function status($status = 0)
    {
        return $this->builder->where('status', $status);
    }

    /**
     * Filter orders by delivery condition 
     * 
     * @param string  $status
     * @return void
     */
    public function delivery($condition = 'today')
    {
        $method = 'delivery' . ucfirst($condition);

        if(method_exists($this, $method))
        {
            $this->$method();
        }
    }

    /**
     * Show orders with expired delivery data 
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function deliveryExpired()
    {
        return $this->builder->where('delivery_dt', '<', Carbon::now());
    }

    /**
     * Show orders waiting to be dispatched 
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function deliveryFuture()
    {
        return $this->builder->where('delivery_dt', '>', Carbon::now());
    }

    /**
     * Show orders waiting to be dispatched 
     * within current day
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function deliveryToday()
    {
        return $this->builder->whereDate('delivery_dt', Carbon::today());
    }

    /**
     * Show orders waiting to be dispatched  
     * within 24 hours
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function deliveryHours24()
    { 
        return $this->builder->whereBetween(
            'delivery_dt', [Carbon::now(), Carbon::now()->addHours(24)
        ]);
    }
}