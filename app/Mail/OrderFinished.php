<?php

namespace App\Mail;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderFinished extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    public $products;

    /**
     * Create a new message instance.
     * 
     * @param \App\Order  $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;

        $this->products = $order->productsInfo();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@ordershipper.com')
                    ->subject('Order has been finished!')
                    ->markdown('emails.orders.finished');
    }
}
