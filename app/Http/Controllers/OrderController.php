<?php

namespace App\Http\Controllers;

use App\Order;
use App\Partner;
use App\Filters\OrderFilters;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    /**
     * Display a listing of orders.
     * 
     * @param \App\Filters\OrderFilters
     * @return \Illuminate\Http\Response
     */
    public function index(OrderFilters $filters)
    {
        $orders = Order::getAll($filters)->load('products');

        return view('orders.index', [
            'orders' => $orders
        ]);
    }

    /**
     * Display a form for updating an order.
     * 
     * @param \App\Order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    { 
        $orderData = Order::getById($order->id)->first();

        return view('orders.edit', [
            'order' => $orderData,
            'products' => $order->productsInfo(),
            'partners' => Partner::all()
        ]);
    }

    /**
     * Update the given order.
     * 
     * @param \App\Order
     * @return \Illuminate\Http\Response
     */
    public function update(Order $order)
    {
        $validatedData = request()->validate([
            'client_email' => ['required', 'email'],
            'partner_id' => ['required', 'exists:partners,id'],
            'status' => ['required', Rule::in($order->getStates())]
        ]);  
        
        // When model is updated the "OrderUpdated" event is fired
        $order->update($validatedData);
        
        return response()->json([], 204);
    }
}
