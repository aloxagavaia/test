<?php

namespace App\Http\Controllers;

use App\YandexWeather;

class WeatherController extends Controller
{
    /**
     * Show the weather for a given town.
     * 
     * @param string  $town
     * @return \Illuminate\Http\Response
     */
    public function show($town)
    {
        $data = (new YandexWeather)->getWeather($town);

        return response()->view('weather.show', [
            'town' => $town,
            'data' => $data,
            'error_message' => ''
        ], 200);
    }
}
